var isLetter;

function generateTable(table, wordLength, nbAttempts) {
    for (let i = 0; i < nbAttempts; i++) {
        let row = table.insertRow();
        for (let j = 0; j < wordLength; j++) {
            let cell = row.insertCell();
            let letter = document.createElement('input');
            letter.classList.add("letter");
            letter.maxLength = 1;

            if (i != 0) {
                letter.disabled = true;
            } else {
                row.classList.add('selectedRow');
            }
            cell.appendChild(letter);
            if (i == 0 && j == 0) {
                letter.focus();
            }
        }
    }
}




let table = document.querySelectorAll("table");

document.body.addEventListener('click', ($event) => {
    if ($event.target.closest('.config') && $event.target.closest("button")) {
        document.querySelector('.config').style.display = "none";
        $event.target.style.display = "none";
        document.querySelector('table').style.display = "flex";
        let wordLength = $event.target.closest('.config').querySelector('.wordConfig').querySelector('input').value;
        let nbAttempts = $event.target.closest('.config').querySelector('.nbAttempsConfig').querySelector('input').value;
        generateTable(table[0], wordLength, nbAttempts);
    }
})

document.body.addEventListener('focus', ($event) => {
    if ($event.target.closest('table')) {
        $event.preventDefault();
    }
})
document.body.addEventListener("input", ($event) => {
    if ($event.target.closest('.wordConfig')) {
        $event.target.closest('.wordConfig').querySelector('p').innerText = $event.target.value;
    }
    if ($event.target.closest('.nbAttempsConfig')) {
        $event.target.closest('.nbAttempsConfig').querySelector('p').innerText = $event.target.value;
    }
});
document.body.addEventListener('keypress', ($event) => {
    if ($event.target.closest('table')) {
        letterOnly($event);
    }
})

document.body.addEventListener('keydown', ($event) => {
    if ($event.target.closest('table')) {
        isLetter = isLetterInInput($event);
    }
})

document.body.addEventListener('keyup', ($event) => {
    if ($event.target.closest('table')) {
        let table = $event.target.closest('table');
        let row = $event.target.closest('tr');
        let cell = $event.target.closest('td');
        if (cell.cellIndex < table.rows[row.rowIndex].cells.length - 1 && $event.target.value != "" && $event.code != "Enter" && $event.key != "Enter") {
            nextCell(table, row, cell);
        } else if (row.rowIndex < table.rows.length - 1 && ($event.code == "Enter" || $event.key == "Enter")) {
            if (isRowFull(table, row)) {
                disableRow(table, row);
                nextRow(table, row);
            }

        } else if (cell.cellIndex > 0 && ($event.code == "Backspace" || $event.key == "Backspace") && !isLetter) {
            previousCell(table, row, cell);
        }
    }
})

function nextCell(table, row, cell) {
    table.rows[row.rowIndex].cells[cell.cellIndex + 1].childNodes[0].focus();
}

function previousCell(table, row, cell) {
    table.rows[row.rowIndex].cells[cell.cellIndex - 1].childNodes[0].focus();
}

function nextRow(table, row) {
    for (let i = 0; i < table.rows[row.rowIndex + 1].cells.length; i++) {
        table.rows[row.rowIndex + 1].cells[i].childNodes[0].disabled = false;
    }
    table.rows[row.rowIndex].classList.remove('selectedRow');
    table.rows[row.rowIndex].classList.add('usedRow');
    table.rows[row.rowIndex + 1].classList.add('selectedRow');
    table.rows[row.rowIndex + 1].cells[0].childNodes[0].focus();
}

function disableRow(table, row) {
    for (let i = 0; i < table.rows[row.rowIndex].cells.length; i++) {
        table.rows[row.rowIndex].cells[i].childNodes[0].disabled = true;
    }

}

function isRowFull(table, row) {
    let isNotEmpty = true;
    for (let i = 0; i < table.rows[row.rowIndex].cells.length; i++) {
        if (table.rows[row.rowIndex].cells[i].childNodes[0].value == "") {
            table.rows[row.rowIndex].cells[i].childNodes[0].classList.add = "inputEmpty";
            isNotEmpty = false;
        }
    }
    return isNotEmpty;

}

function isLetterInInput($event) {
    if ($event.target.value != "") {
        return true;
    } else {
        return false;
    }
}

function letterOnly($event) {
    let char = String.fromCharCode($event.keyCode); // Get the character
    if (/^[A-Za-z]+$/.test(char)) return true;
    // Match with regex
    else $event.preventDefault();
}